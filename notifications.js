/*
 * RSyncUI
 * An extension to synchronize with rsync
 *
 * This file is part of RSyncUI
 *
 * RSyncUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RSyncUI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the extension.
 * If not, see <http://www.gnu.org/licenses/>.
 */

const Lang = imports.lang;

const St = imports.gi.St;
const Gtk = imports.gi.Gtk;
const Shell = imports.gi.Shell;
const Clutter = imports.gi.Clutter;

const ModalDialog = imports.ui.modalDialog;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Settings = Extension.imports.settings;

const _ = Settings.getGettext();


var SynchronizeInformationDialog = new Lang.Class({
    Name: 'SynchronizeInformationDialog',
    Extends: ModalDialog.ModalDialog,

    _indicator: null,

    _init(indicator) {
        this.parent({ styleClass: 'extension-dialog' });

        this._indicator = indicator;

        let lastSync = Settings.getHumanReadableDateOfPreviousSync();
        let nextSync = Settings.getHumanReadableDateOfNextSchedule();
        let lastError = Settings.getLastErrors();

        let hasErrors = (lastError != null && lastError.length > 0);
        let hasSchedule = (nextSync != null && nextSync != "");


        let buttons = [];
        buttons.push({
            label:  _("Preferences"),
            action:  this._onPrefsButtonPressed.bind(this)
        });
        if (hasErrors) buttons.push({
            label:  _("Try again"),
            action:  this._onSyncButtonPressed.bind(this)
        });
        buttons.push({
            label: _("Close"),
            action: this._onCancelButtonPressed.bind(this),
            key:    Clutter.Escape,
            default: true
        });
        this.setButtons(buttons);

        // main box
        let box = new St.BoxLayout({ style_class: 'message-dialog-main-layout',
                                     vertical: true });
        this.contentLayout.add(box);

        // header (icon + title)
        let headerBox = new St.BoxLayout({ vertical: false });
        box.add(headerBox);

        if (hasErrors) headerBox.add(new St.Icon({ icon_name: Settings.icons.ERROR }));
        else headerBox.add(new St.Icon({ icon_name: Settings.icons.INFO }));
        headerBox.add(new St.Label({ style_class: 'message-dialog-title headline',
                                     text:  "  " + _("Information about synchronization"),
                                     style: "margin: 6px; padding: 6px; color: #aaaaaa;" }));

        // last sync date
        let lastSyncBox = new St.BoxLayout({ vertical: true });
        box.add(lastSyncBox);

        lastSyncBox.add(new St.Label({ text: _("Most recently synchronized") + ":",
                                       style: "color: #aaaaaa;" }));
        lastSyncBox.add(new St.Label({ text: lastSync,
        style: "font-weight: bold;" }));

        // show errors, if there are errors
        if (hasErrors) {
            let errorBox = new St.BoxLayout({ vertical: true });
            box.add(errorBox);

            errorBox.add(new St.Label({ text: _("Errors during last synchronization") + ":",
                                        style: "color: #aaaaaa;" }));

            let errorListBox = new St.BoxLayout({ vertical: true });
            errorBox.add(errorListBox);
            for (let i=0; i<lastError.length; i++) {
                errorListBox.add(new St.Label({ text: lastError[i] }));
            }
        }

        // show next scheduled, if there is one
        if (hasSchedule) {
            let nextSyncBox = new St.BoxLayout({ vertical: true });
            box.add(nextSyncBox);

            nextSyncBox.add(new St.Label({ text: _("Upcoming synchronization") + ":",
                                           style: "color: #aaaaaa;" }));
            nextSyncBox.add(new St.Label({ text: nextSync,
            style: "font-weight: bold;" }));
        }
    },

    _onCancelButtonPressed(button, event) {
        this.close();
    },

    _onSyncButtonPressed(button, event) {
        this.close();
        this._indicator.syncButton.synchronize();
    },

    _onPrefsButtonPressed(button, event) {
        this.close();
        this._indicator.prefsButton.showPreferences();
    }
});
