/*
 * RSyncUI
 * An extension to synchronize with rsync
 *
 * This file is part of RSyncUI
 *
 * RSyncUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RSyncUI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the extension.
 * If not, see <http://www.gnu.org/licenses/>.
 */

const Lang = imports.lang;

const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Settings = Extension.imports.settings;

const _ = Settings.getGettext();


var Synchronization = new Lang.Class({
	Name: 'Synchronization',

    _enabledGroupList: null,
    _pingCount: 0,
    _childCount: 0,
    _childsRunning: 0,
    _errorReader: [],
    _childWatch: [],

    _rsyncPath: null,

    _callback: null,


	_init() {
        this._enabledGroupList = Settings.getEnabledGroups();
		this._rsyncPath = GLib.find_program_in_path("rsync");

        Settings.clearLastErrorLogs();

		if (this._rsyncPath == null) this._throwError(_("\"rsync\" could not be found."));
        if (this._enabledGroupList.length <= 0) this._throwError(_("No groups to synchronize."));
    },

    synchronize(callback) {
        this._callback = callback;

		this._childCount = 0;
		this._childsRunning = 0;

		this._errorReader = [];
		this._childWatch = [];
        this._inputReader = [];
        this._outputWriter = [];

		for (var i = 0; i < this._enabledGroupList.length; i++) {
            this.synchronizeGroup(this._enabledGroupList[i]);
		}

        this._updateLastSynchronizationDate();
    },

    synchronizeGroup(groupName) {
        let type = Settings.getDestinationType(groupName);
		let server = Settings.getServer(groupName);
        let mac = Settings.getMac(groupName);
        let user = Settings.getUserName(groupName);

        if (type != null && type != "" && type != "local" && mac != null && mac != "") {
            this._wakeAndCall(server, user, mac, Lang.bind(this, () => {
                this._synchronizeGroup(groupName);
                // this._childsRunning--;
            }));
        } else if (type != "local") {
            this._testSshAccess(server, user, () => {
                this._synchronizeGroup(groupName);
            });
        } else {
            this._synchronizeGroup(groupName);
        }
    },

    _synchronizeGroup(groupName) {
		let sources = Settings.getSources(groupName);
		let destination = Settings.getDestination(groupName);

        if (destination == null || destination == "") {
            Settings.addError(_("Group") + ' "' + groupName + '": ' + _("has no destination configured!"));
            return;
        }

        if (sources.length <= 0) {
            Settings.addError(_("Group") + ' "' + groupName + '": ' + _("has no source configured!"));
            return;
        }

        for (let i=0; i<sources.length; i++) {
            this._childCount++;
            this._childsRunning++;

            let argv = this.getCmdArray(Settings.rsync, Settings.getCustomOptions(groupName));

            if (Settings.getDeleteOption(groupName)) argv.push(Settings.rsync.DELETE);
            if (Settings.getCompressOption(groupName)) argv.push(Settings.rsync.COMPRESS);

            argv.push(sources[i]);
            argv.push(destination);

            let [res, pid, in_fd, out_fd, err_fd] = GLib.spawn_async_with_pipes(
                null, argv, null, GLib.SpawnFlags.DO_NOT_REAP_CHILD, null
            );

            // this._inputReader[this._childCount] = new Gio.DataInputStream({
            //     base_stream: new Gio.UnixInputStream({fd: in_fd})
            // });
            // this._inputReader[this._childCount].read_upto_async(
            //     // 0, null, (input_reader, async_res, user_data) => { // read_line_async
            //     "@", 1, null, null, (input_reader, async_res, ir) => { // read_upto_async
            //         print("reading ...");
            //             print(input_reader);
            //                 print(async_res);
            //                     print(ir);
            //     	let [lineout, charlength, err] = input_reader.read_upto_finish(async_res);
            //     	if (lineout != null) print(_("Group") + ' "' + groupName + '": ' + String.fromCharCode(lineout));
            //     	// if (lineout != null) print(_("Group") + ' "' + groupName + '": ' + this._utf8ArrayToStr(lineout));
            //     });

            this._outputWriter[this._childCount] = new Gio.DataOutputStream({
                base_stream: new Gio.UnixOutputStream({fd: out_fd})
            });
            // this._outputWriter[this._childCount].read_upto_async(
            //     // -1, null, (input_reader, async_res, user_data) => { // read_line_async
            //     ":", -1, null, null, (input_reader, async_res, user_data) => { // read_upto_async
            //         print("reading ...");
            //     	let [lineout, charlength, err] = input_reader.read_line_finish(async_res);
            //     	if (lineout != null) print(_("Group") + ' "' + groupName + '": ' + this._utf8ArrayToStr(lineout));
            //     });

            this._errorReader[this._childCount] = new Gio.DataInputStream({
                base_stream: new Gio.UnixInputStream({fd: err_fd})
            });
            this._errorReader[this._childCount].read_line_async(
                0, null, (gobject, async_res, user_data) => {
                	let [lineout, charlength, err] = gobject.read_line_finish(async_res);
                    if (lineout != null) {
                        let error = "";
                        for (let i=0; i<lineout.length; i++) error += String.fromCharCode(lineout[i]);
                    }
                	if (lineout != null) Settings.addError(_("Group") + ' "' + groupName + '": ' + error);
                });

            this._childWatch[this._childCount] = GLib.child_watch_add(
                GLib.PRIORITY_DEFAULT, pid, Lang.bind(this, (childWatch, state, nChild) => {
                    this._childsRunning--;

                    if (this._childsRunning <= 0) this._callback();

                    GLib.source_remove(this._childWatch[nChild]);
                    this._errorReader[nChild].close(null);
                }, this._childCount)
            );
        }
    },

    _updateLastSynchronizationDate() {
        // Update the last-sync date after synchro
		let date = new GLib.DateTime();
		let minutes = date.get_minute();
		minutes = (minutes < 9 ) ? "0" + minutes : minutes;

        Settings.setLastSync(date.get_day_of_month() + "/"
                           + date.get_month()        + "/"
                           + date.get_year()         + " - "
                           + date.get_hour()         + ":"
                           + minutes);
    },

    _wakeAndCall(addres, user, mac, callFunction) {
        this.executeWhenAvailable(addres, user, callFunction);

        let args = this.getCmdArray(Settings.wol);
        args.push(mac);

        // TODO : error catching
        for (let i=0; i<3; i++) {
            GLib.timeout_add_seconds(0, i*3, () => {
                let [res, pid, in_fd, out_fd, err_fd] = GLib.spawn_async_with_pipes(
                    null, args, null, GLib.SpawnFlags.DEFAULT, null
                );
            });
        }
    },

    executeWhenAvailable(addres, user, execFunction) {
        this._childsRunning++;

        let args = this.getCmdArray(Settings.ping);
        args.push(addres);

        // TODO : error catching
        this._pingCount++;
        if (this._pingCount <= Settings.ping.MAX_PINGS) {
            let [res, pid, in_fd, out_fd, err_fd] = GLib.spawn_async_with_pipes(
                null, args, null, GLib.SpawnFlags.DO_NOT_REAP_CHILD, null
            );

            GLib.child_watch_add(GLib.PRIORITY_DEFAULT, pid, Lang.bind(this, function(childWatch, state, nChild) {
                if (state == 0) {
                    this._pingCount = 0;
                    this._testSshAccess(addres, user, execFunction);
                    // execFunction();
                } else {
                    this.executeWhenAvailable(addres, user, execFunction);
                }
                this._childsRunning--;
            }));
        } else {
            this._pingCount = 0;
            this._childsRunning--;

            this._throwError(_("Host") + ' "' + addres + '" ' + _("is not accessible."));
        }
    },

    _testSshAccess(addres, user, execFunction) {
        this._childsRunning++;

        let ssh_test_args = this.getCmdArray(Settings.ssh);
        ssh_test_args.push(user + "@" + addres);
        ssh_test_args.push("exit");

        let [res, pid, in_fd, out_fd, err_fd] = GLib.spawn_async_with_pipes(
            null, ssh_test_args, null, GLib.SpawnFlags.DO_NOT_REAP_CHILD, null
        );
        GLib.child_watch_add(GLib.PRIORITY_DEFAULT, pid, Lang.bind(this, (pid, status, nChild) => {
            if (status == 0) {
                execFunction();
            } else this._throwError(
                ("Host") + ' "' + addres + '" ' + _("does not grant access to user")
                 + ' "' + user + '" ' + _("(probably no key pair has been configured).")
            );

            this._childsRunning--;
        }));
    },

    getCmdArray(cmdSettings, customOptions="") {
        let argv = [ GLib.find_program_in_path(cmdSettings.CMD) ];

        if (customOptions != null && customOptions != "")
            argv = argv.concat(customOptions.split(" "));
        else if (cmdSettings.DEFAULT != null && cmdSettings.DEFAULT != "")
            argv = argv.concat(cmdSettings.DEFAULT.split(" "));

        return argv;
    },

    _throwError(error) {
        this._childsRunning--;

        Settings.addError(error);
        if (this._childsRunning == 0) this._callback();
        throw error;
    }
});
