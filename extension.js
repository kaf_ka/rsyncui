/*
 * RSyncUI
 * An extension to synchronize with rsync
 *
 * This file is part of RSyncUI
 *
 * RSyncUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RSyncUI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the extension.
 * If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * TODO:
 + - english correction
 * - translation
 * - passwords
 * - some error handling (like commands are available)
 * - some cleanup
 */

const Lang = imports.lang;

const St = imports.gi.St;
const Gio = imports.gi.Gio;
const Shell = imports.gi.Shell;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
const Animation = imports.ui.animation;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Settings = Extension.imports.settings;
const Scheduler = Extension.imports.scheduler;
const Synchronize = Extension.imports.synchronize;
const Notifications = Extension.imports.notifications;

const _ = Settings.getGettext();


var _indicator;
var _schedulerUtils;


var SyncButton = new Lang.Class({
	Name: 'SyncButton',
	Extends: PopupMenu.PopupBaseMenuItem,

	_init () {
		this.parent({
			reactive: false
		});

        let icon = new St.Icon({
            icon_name: Settings.icons.SYNC,
            icon_size: Settings.icons.size.MEDIUM
        });

        let button = new St.Button({
            style_class: 'system-menu-action popup-inactive-menu-item',
            child: icon,
			// reactive: true
        });

        icon.set_style('padding: 12px');
        button.set_style('padding: 0px');

        this.actor.add(button, {
            expand: true,
            x_fill: false,
            x_align: St.Align.MIDDLE
        });

        button.connect("clicked", Lang.bind(this, this.synchronize));
    },

    synchronize() {
        try {
            Main.notify(Extension.metadata.name.toString(), _("RSyncUI has started a new synchronization."));
            _indicator.setAnimation(true);

            let sync = new Synchronize.Synchronization();
            sync.synchronize(() => {
                //End notification
                let endErrors = Settings.getLastErrors();
                if (endErrors.length == 0) {
                    Main.notify(Extension.metadata.name.toString(), _("RSyncUI has completed the synchronization without any problems."));
                } else {
                    Main.notifyError(Extension.metadata.name.toString(), _("Unfortunately, RSyncUI has encountered problems while running the last command!"));
                }

                _indicator.setAnimation(false);
            });
        } catch (error) {
            print(error);
            Main.notifyError(Extension.metadata.name.toString(),_("Unfortunately, RSyncUI has encountered problems while running the last command!"));
            _indicator.setAnimation(false);
        }
    }
});

var PreferencesButton = new Lang.Class({
	Name: 'PreferencesButton',
	Extends: PopupMenu.PopupBaseMenuItem,

    _dialog: null,
    _infoIcon: null,
    _infoButton: null,

	_init() {
		this.parent({
			reactive: false
		});

        this._infoIcon = null;
        this._infoButton = new St.Button({
            style_class: 'system-menu-action popup-inactive-menu-item'
        });

        this.setAttentionIcon((Settings.getLastErrors().length > 0));

        this.actor.add(this._infoButton, {
            expand: true,
            x_fill: false,
            x_align: St.Align.START
        });

        let prefsIcon = new St.Icon({
            icon_name: Settings.icons.PREFERENCES,
            icon_size: Settings.icons.size.SMALL
        });

        let prefsButton = new St.Button({
            style_class: 'system-menu-action popup-inactive-menu-item',
            child: prefsIcon,
        });
        prefsIcon.set_style('padding: 4px');
        prefsButton.set_style('padding: 4px');

        this.actor.add(prefsButton, {
            expand: true,
            x_fill: false,
            x_align: St.Align.END
        });

        Settings.gsettings.main.connect("changed::" + Settings.keys.scheduler.LAST_ERRORS, Lang.bind(this, () => {
            this.setAttentionIcon((Settings.getLastErrors().length > 0));
        }));

        this._infoButton.connect("clicked", this.showInfo);
        prefsButton.connect("clicked", this.showPreferences);
    },

    showInfo() {
        if (this._dialog != null) {
            this._dialog.destroy()
            this._dialog = null;
        }

        this._dialog = new Notifications.SynchronizeInformationDialog(_indicator);
        this._dialog.open();
    },

    showPreferences() {
        let app_system = Shell.AppSystem.get_default();
        let gnome_shell_extension_prefs = app_system.lookup_app('gnome-shell-extension-prefs.desktop');

        if (gnome_shell_extension_prefs.get_state() == gnome_shell_extension_prefs.SHELL_APP_STATE_RUNNING) {
            gnome_shell_extension_prefs.activate();

        } else {
            let info = gnome_shell_extension_prefs.get_app_info();
            let timestamp = global.display.get_current_time_roundtrip();
            info.launch_uris([Extension.metadata.uuid], global.create_app_launch_context(timestamp, -1));
        }
    },

    setAttentionIcon(attention=true) {
        if (this._infoIcon != null) this._infoIcon.destroy();

        if (attention) {
            this._infoIcon = new St.Icon({
                icon_name: Settings.icons.ERROR,
                icon_size: Settings.icons.size.SMALL
            });

            this._infoIcon.set_style('padding: 4px');
            this._infoButton.set_style("padding: 4px");
            // this._infoButton.set_style("padding: 4px; background-color: rgba(255, 30, 30, 1)");

            this._infoButton.set_child(this._infoIcon);
        } else {
            this._infoIcon = new St.Icon({
                icon_name: Settings.icons.INFO,
                icon_size: Settings.icons.size.SMALL
            });

            this._infoIcon.set_style('padding: 4px');
            this._infoButton.set_style("padding: 4px");

            this._infoButton.set_child(this._infoIcon);
        }

        this._infoButton.show_all();
    }
});

var RSyncIndicator = new Lang.Class({ //the main menu
	Name: 'RSyncIndicator',
	Extends: PanelMenu.Button,

    _icon: null,
    _iconBox: null,
    _groupSwitches: {},
    _groupSignals: {},

    syncButton: null,
    prefsButton: null,


	_init() {
		this.parent(0.0, Settings.indicator.NAME);

        this._iconBox = new St.BoxLayout();
        this.setAnimation(false);

        let panelBox = new St.BoxLayout({
            style_class: 'panel-status-menu-box'
        });
        panelBox.add_child(this._iconBox);
        panelBox.add_child(PopupMenu.arrowIcon(St.Side.BOTTOM));
        this.actor.add_actor(panelBox);

        this._createMenu();
    },

    setAnimation(animated=true) {
        if (this._icon != null) this._icon.destroy();
        this._iconBox.show_all();

        if (animated) {
            let spinnerIcon = Gio.File.new_for_uri('resource:///org/gnome/shell/theme/process-working.svg');
            let spinner = new Animation.AnimatedIcon(spinnerIcon, Settings.icons.size.SMALL);

            this._icon = spinner.actor;
            this._iconBox.add_actor(spinner.actor);

            spinner.play();
            spinner.actor.show();
        } else {
            this.lastError = Settings.getLastErrors();

            let iconName;
            if (this.lastError != null && this.lastError.length > 0) iconName = Settings.icons.ERROR;
            else iconName = Settings.icons.PANEL;

            this._icon = new St.Icon({
                style_class: 'system-status-icon indicator-item',
                icon_name: iconName
            });

            this._iconBox.add_child(this._icon);
        }

        this._iconBox.show_all();
    },

    _createMenu() {
        this.syncButton = new SyncButton();
        this.menu.addMenuItem(this.syncButton);

        this.prefsButton = new PreferencesButton();
        this.menu.addMenuItem(this.prefsButton);

        this._refreshGroupSwitches();
        Settings.gsettings.grouplist.connect(
            "changed::" + Settings.keys.LIST,
            Lang.bind(this, this._refreshGroupSwitches)
        );
    },

    _refreshGroupSwitches() {
        Object.keys(this._groupSwitches).forEach(Lang.bind(this, (groupName, index) => {
            Settings.gsettings.groups[groupName].disconnect(this._groupSignals[groupName]);
            this._groupSwitches[groupName].destroy();
        }));

		let groupsList = Settings.getGroups();
		this._groupSwitches = {};
		this._groupSignals = {};

		if (groupsList.length <= 0) {
            this._groupSwitches.nothing = new PopupMenu.PopupMenuItem(_("There is nothing configured yet."));
            this.menu.addMenuItem(this._groupSwitches.nothing);

		} else {
			for (var i = 0; i < groupsList.length; i++) {
                let groupName = groupsList[i];

                if (Settings.gsettings.groups[groupName] == null)
                    Settings.initGroupSetting(groupName);

				this._groupSwitches[groupName] = new PopupMenu.PopupSwitchMenuItem(groupName);
                this._groupSwitches[groupName].setToggleState(Settings.isGroupEnabled(groupName));
				this.menu.addMenuItem(this._groupSwitches[groupName]);

				this._groupSwitches[groupName].connect(
                    'toggled',
                    Lang.bind(this, (groupSwitch, state, groupName) => {
                        Settings.enableGroup(groupName, state);
                    }, groupName)
                );
                this._groupSignals[groupName] = Settings.gsettings.groups[groupsList[i]].connect(
                    "changed::" + Settings.keys.group.ENABLE,
                    Lang.bind(this, (settings, state, groupName) => {
                        this._groupSwitches[groupName].setToggleState(Settings.isGroupEnabled(groupName));
                    }, groupName)
                );
			}
		}
	},

	destroy() {
		this.parent();
	}

});

function init() {
    // initialize common settings
	Settings.init();
}

function enable() {
	_indicator = new RSyncIndicator();
	Main.panel.addToStatusArea(Settings.indicator.NAME, _indicator, 1, Settings.indicator.LOCATION);

	_schedulerUtils = new Scheduler.SchedulerUtils();
    _schedulerUtils.setCallback(() => { _indicator.syncButton.synchronize(); });
    if (Settings.isSchedulerEnabled()) _schedulerUtils.start();
}

function disable() {
	_indicator.destroy();
    _schedulerUtils.stop();
}
