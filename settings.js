/*
 * RSyncUI
 * An extension to synchronize with rsync
 *
 * This file is part of RSyncUI
 *
 * RSyncUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RSyncUI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the extension.
 * If not, see <http://www.gnu.org/licenses/>.
 */

const Lang = imports.lang;

const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;
const GLib = imports.gi.GLib;

const Extension = imports.misc.extensionUtils.getCurrentExtension();

var folders = {
    ICONS: "icons",
    SCHEMA: "schemas",
    LOCALE: "locale"
}

// translation
imports.gettext.bindtextdomain(
    Extension.metadata['gettext-domain'],
    Extension.dir.get_child(folders.LOCALE).get_path()
);

const _ = getGettext();


var gsettings = {};

var schemas = {
    Extension: {
        ID:   "org.gnome.shell.extensions.rsyncui",
        PATH: "/org/gnome/shell/extensions/rsyncui/"
    },
    GroupList: {
        ID:   "org.gnome.shell.extensions.rsyncui.groupList",
        PATH: "/org/gnome/shell/extensions/rsyncui/groups/"
    },
    Group: {
        ID:   "org.gnome.shell.extensions.rsyncui.group",
        PATH: "/org/gnome/shell/extensions/rsyncui/groups/"
    }
};

// var textdomain = "rsyncui";


var keys = {
    LIST: "list",

    group: {
        SOURCES:        "sources",
        DESTINATION:    "destination",
        ENABLE:         "enable",
        DELETE:         "delete",
        COMPRESS:       "compress",
        CUSTOM_OPTIONS: "custom-options",
        DEST_TYPE:      "type",
        DEST_SERVER:    "server",
        DEST_USER:      "user",
        DEST_PORT:      "port",
        DEST_FOLDER:    "folder",
        DEST_WOL:       "wake-on-lan"
    },

    scheduler: {
        DAYS:        "days",
        TIME:        "time",
        NEXT_SYNC:   "next-sync",
        LAST_SYNC:   "last-sync",
        LAST_ERRORS: "last-errors"
    },

};

// Icons
var icons = {
    size: {
        SMALL:  16,
        NORMAL: 24,
        MEDIUM: 48,
        LARGE:  96,
    },

    EXTENSION:   "folder-remote",
    PANEL:       "folder-remote-symbolic",
    SYNC:        "emblem-synchronizing-symbolic",
    PREFERENCES: "preferences-system-symbolic",
    INFO:        "dialog-information-symbolic",
    ERROR:       "dialog-warning-symbolic",
}

var days = [
    _('Monday'),
    _('Tuesday'),
    _('Wednesday'),
    _('Thursday'),
    _('Friday'),
    _('Saturday'),
    _('Sunday')
];

var months = [
    _('January'),
    _('February'),
    _('March'),
    _('April'),
    _('May'),
    _('June'),
    _('July'),
    _('August'),
    _('September'),
    _('October'),
    _('November'),
    _('December')
];

var indicator = {
    NAME:     "rsyncui",
    LOCATION: "right"
}

var ping = {
    CMD:      'ping',
    DEFAULT:  '-c1',
    MAX_PINGS: 15
}

var wol = {
    CMD:     'wol',
    DEFAULT: ''
}

var rsync = {
    CMD:      'rsync',
    DEFAULT:  '-rlptq',
    COMPRESS: '--compress',
    DELETE:   '--delete'
}

var ssh = {
    CMD:      'ssh',
    DEFAULT:  '-q -o PasswordAuthentication=no'
}


function init() {
    // icons
	// Gtk.IconTheme.get_default().append_search_path(
    //     Extension.dir.get_child(folders.ICONS).get_path()
    // );

    // gsettings
    gsettings.main = _getSettings(
        schemas.Extension.ID
    );

    gsettings.grouplist = _getSettings(
        schemas.GroupList.ID,
        schemas.GroupList.PATH
    );

    gsettings.groups = {};
    let groups = getGroups();
    for (var i = 0, len = groups.length; i < len; i++)
        initGroupSetting(groups[i]);
}

function getGettext() {
    return imports.gettext.domain(Extension.metadata['gettext-domain']).gettext;
}

function _getSettings(schema, path=null) {
    let schemaSource = Gio.SettingsSchemaSource.new_from_directory(
        Extension.dir.get_child(folders.SCHEMA).get_path(),
        Gio.SettingsSchemaSource.get_default(),
        false
    );

    let schemaObj = schemaSource.lookup(schema, true);

    let preferences = { settings_schema: schemaObj };
    if (path != null) preferences.path = path;

    return new Gio.Settings(preferences);
}

function initGroupSetting(groupName) {
    gsettings.groups[groupName] = _getSettings(
        schemas.Group.ID,
        schemas.Group.PATH + groupName + "/"
    );
}

function getGroups() {
    return gsettings.grouplist.get_strv(keys.LIST);
}

function setGroups(groupList) {
    gsettings.grouplist.set_strv(keys.LIST, groupList);
}

function isGroupEnabled(groupName) {
    return gsettings.groups[groupName].get_boolean(keys.group.ENABLE);
}

function enableGroup(groupName, enabled) {
    gsettings.groups[groupName].set_boolean(keys.group.ENABLE, enabled);
}

function getEnabledGroups() {
    let result = [];
    Object.keys(gsettings.groups).forEach(Lang.bind(this, (groupName, index) => {
        if (gsettings.groups[groupName].get_boolean(keys.group.ENABLE)) {
            result.push(groupName);
        }
    }));
    return result;
}

function getSources(groupName) {
    return gsettings.groups[groupName].get_strv(keys.group.SOURCES);
}

function getDestinationType(groupName) {
    return gsettings.groups[groupName].get_string(keys.group.DEST_TYPE);
}

function getDestinationFolder(groupName) {
    return gsettings.groups[groupName].get_string(keys.group.DEST_FOLDER);
}

function setDestinationFolder(groupName, path) {
    gsettings.groups[groupName].set_string(keys.group.DEST_FOLDER, path);
}

function getDestination(groupName) {
    let type = gsettings.groups[groupName].get_string(keys.group.DEST_TYPE);
    let result;

    if (type == "local") {
        result = gsettings.groups[groupName].get_string(keys.group.DEST_FOLDER);
    } else {
        let username = gsettings.groups[groupName].get_string(keys.group.DEST_USER);
        if (username != null && username != "") result = username + "@";

        let server = gsettings.groups[groupName].get_string(keys.group.DEST_SERVER);
        result += server + ":";

        let port = gsettings.groups[groupName].get_int(keys.group.DEST_PORT);
        if (port != null && port != 0) result = port;

        let folder = gsettings.groups[groupName].get_string(keys.group.DEST_FOLDER);
        result += "/" + folder;
    }

    return result;
}

function _convertStringToDateTime(dateString) {
    if (dateString != null && dateString != "") {
        // dateFormat = "%s/%s/%s - %s:%s";
        let [nsd, nst] = dateString.split(" - ");
        let [day, month, year] = nsd.split("/");
        let [hour, minutes] = nst.split(":");

        return GLib.DateTime.new_local(year, month, day, hour, minutes, 0);
    }

    return null;
}

function _makeHumanReadableDate(date) {
    if (date == null) return "";

    let dayOfTheWeek = days[date.get_day_of_week() - 1];
    let month = months[date.get_month() - 1];
    let minutes = date.get_minute();
    minutes = (minutes <= 9 ) ? "0" + minutes : minutes;

    return dayOfTheWeek            + ", "
         + date.get_day_of_month() + " "
         + month.toLowerCase()     + " at "
         + date.get_hour()         + ":"
         + minutes;
}

function getServer(groupName) {
    return gsettings.groups[groupName].get_string(keys.group.DEST_SERVER);
}

function getUserName(groupName) {
    return gsettings.groups[groupName].get_string(keys.group.DEST_USER);
}

function getMac(groupName) {
    return gsettings.groups[groupName].get_string(keys.group.DEST_WOL);
}

function getLastErrors() {
    return gsettings.main.get_strv(keys.scheduler.LAST_ERRORS);
}

function setLastErrors(errorList) {
    gsettings.main.set_strv(keys.scheduler.LAST_ERRORS, errorList);
}

function addError(error) {
    let errorList = getLastErrors();
    errorList.push(error);
    setLastErrors(errorList);
}

function clearLastErrorLogs() {
    setLastErrors([]);
}

function getLastSync() {
    return gsettings.main.get_string(keys.scheduler.LAST_SYNC);
}

function getHumanReadableDateOfPreviousSync() {
    let date = _convertStringToDateTime(getLastSync());
    if (date == null) return "";
    return _makeHumanReadableDate(date);
}

function setLastSync(date) {
    gsettings.main.set_string(keys.scheduler.LAST_SYNC, date);
}

function getNextSync() {
    return gsettings.main.get_string(keys.scheduler.NEXT_SYNC);
}

function findDateOfNextSchedule() {
    let scheduledDays = getScheduledDays();
    let scheduledTime = getScheduledTime();
    let nextScheduleDate = null;

    let now = new GLib.DateTime();

    if (scheduledDays.length > 0) {
        for (var i = 0; i < scheduledDays.length; i++) {
            let scheduleDate = GLib.DateTime.new_local(
                now.get_year(),
                now.get_month(),
                now.get_day_of_month() + days.indexOf(scheduledDays[i]) - now.get_day_of_week() + 1,
                scheduledTime.hour,
                scheduledTime.min,
                0
            );
            if (scheduleDate.difference(now) <= 0) scheduleDate = scheduleDate.add_days(7);

            if (nextScheduleDate == null || scheduleDate.difference(nextScheduleDate) < 0)
                nextScheduleDate = scheduleDate;
        }
    }

    return nextScheduleDate;
}

function getDateOfNextSchedule() {
    let date = gsettings.main.get_string(keys.scheduler.NEXT_SYNC);
    return _convertStringToDateTime(date);
}

function getHumanReadableDateOfNextSchedule() {
    let date = getDateOfNextSchedule();
    if (date == null) return "";
    return _makeHumanReadableDate(date);
}

function setNextSync(date) {
    gsettings.main.set_string(keys.scheduler.NEXT_SYNC, date);
}

function setDateOfNextSchedule(date) {
    let minutes = date.get_minute();
    minutes = (minutes <= 9 ) ? "0" + minutes : minutes;

    // let dateFormat = "%s/%s/%s - %s:%s";
    gsettings.main.set_string(keys.scheduler.NEXT_SYNC,
                         date.get_day_of_month() + "/"
                       + date.get_month()        + "/"
                       + date.get_year()         + " - "
                       + date.get_hour()         + ":"
                       + minutes);
}

function getCustomOptions(groupName) {
    return gsettings.groups[groupName].get_string(keys.group.CUSTOM_OPTIONS);
}

function getDeleteOption(groupName) {
    return gsettings.groups[groupName].get_boolean(keys.group.DELETE);
}

function getCompressOption(groupName) {
    return gsettings.groups[groupName].get_boolean(keys.group.COMPRESS);
}

function isSchedulerEnabled() {
    return (getScheduledDays().length > 0);
}

function getScheduledDays() {
    return gsettings.main.get_strv(keys.scheduler.DAYS);
}

function setScheduledDays(dayList) {
    gsettings.main.set_strv(keys.scheduler.DAYS, dayList);
}

function getScheduledTime() {
    let time = gsettings.main.get_string(keys.scheduler.TIME).split(":");
    return { hour: parseInt(time[0]), min: parseInt(time[1]) };
}

function setScheduledTime(hour, minutes) {
    let time;
    if (minutes < 10) time = hour + ":0" + minutes;
    else time = hour + ":" + minutes;

    gsettings.main.set_string(keys.scheduler.TIME, time);
}
