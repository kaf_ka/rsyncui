# rsyncui
This extension helps synchronizing across different folders (both local and
remote). The most important functions are:

- "rsync" is used as a backend. The default options are: "-rlptq".
- There are groups, which contains multiple sources and a destination.
- The destination can be either local or remote (with rysnc).
- There is no support for passwords (yet?), so a key pair must be
    installed for a remote destination.
- Wake-on-lan is available for the destination. (This only works if the
    command "wol" exists.)
- There is a simple possibility to schedule a synchronization. If this
    occurs at a time when the computer is off, it will be executed when
    the computer is turned on again.

The operation of this extension is very similar to "SincroDir". The most
important differences are mentioned above.
