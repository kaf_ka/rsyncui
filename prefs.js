/*
 * RSyncUI
 * An extension to synchronize with rsync
 *
 * This file is part of RSyncUI
 *
 * RSyncUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RSyncUI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the extension.
 * If not, see <http://www.gnu.org/licenses/>.
 */

const Lang = imports.lang;

const Gtk = imports.gi.Gtk;
const Gdk = imports.gi.Gdk;
const GLib = imports.gi.GLib;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Settings = Extension.imports.settings;
const PreferencesWidget = Extension.imports.preferenceswidget;

const _ = Settings.getGettext();


function init() {
    // initialize common settings
    Settings.init();
}

var AboutWidget = new Lang.Class({
    Name: "AboutWidget",
    Extends: Gtk.Grid,

    _init() {
        this.parent({
            margin_bottom: 18,
            row_spacing: 8,
            hexpand: true,
            halign: Gtk.Align.CENTER,
            orientation: Gtk.Orientation.VERTICAL
        });

        let aboutIcon = new Gtk.Image({
            icon_name: Settings.icons.EXTENSION,
            pixel_size: Settings.icons.size.LARGE
        });
        this.add(aboutIcon);

        let aboutName = new Gtk.Label({
            label: "<b>" + Extension.metadata.name.toString() + "</b>",
            use_markup: true
        });
        this.add(aboutName);

        let aboutVersion = new Gtk.Label({ label: _("Version") + ": " + Extension.metadata.version.toString() });
        this.add(aboutVersion);

        let aboutDescription = new Gtk.Label({
            label:  Extension.metadata.description
        });
        this.add(aboutDescription);

        let aboutWebsite = new Gtk.Label({
            label: '<a href="%s">%s</a>'.format(
                Extension.metadata.url,
                Extension.metadata.url
            ),
            use_markup: true
        });
        aboutWebsite.set_padding(15, 15);
        this.add(aboutWebsite);

        // let aboutCopyright = new Gtk.Label({
        //     label: "<small>" + _('Copyleft 2018') + "</small>",
        //     use_markup: true
        // });
        // this.add(aboutCopyright);

        let aboutLicense = new Gtk.Label({
            label: "<small>" +
            "This program is free software: you can redistribute it and/or modify it under" + "\n" +
            "the terms of the GNU General Public License as published by the Free Software" + "\n" +
            "Foundation, either version 3 of the License, or (at your option) any later" + "\n" +
            "version." + "\n" +
            "" + "\n" +
            "This program is distributed in the hope that it will be useful, but WITHOUT ANY" + "\n" +
            "WARRANTY; without even the implied warranty of MERCHANTABILITY or " + "\n" +
            "FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License" + "\n" +
            "for more details." + "\n" +
            "" + "\n" +
            "You should have received a copy of the GNU General Public License along with " + "\n" +
            "this program." + "\n" +
            "If not, see <a href=\"https://www.gnu.org/licenses/\">https://www.gnu.org/licenses/</a>." + "\n" +
            "</small>",
            use_markup: true,
            justify: Gtk.Justification.CENTER
        });
        aboutLicense.get_style_context().add_class("frame");
        aboutLicense.set_padding(15, 15);
        this.add(aboutLicense);
    }
});

var RSyncUIPreferencesWidget = new Lang.Class({
    Name: "RSyncUIPreferencesWidget",
    Extends: PreferencesWidget.Stack,

    _init() {
        this.parent();

        Gtk.IconTheme.get_default().append_search_path(
            Extension.dir.get_child('icons').get_path());


        // synchronization groups page (notebook)
        let synchronizationPage = this.addPage("synchronization", _("Synchronize"), {
            vscrollbar_policy: Gtk.PolicyType.NEVER
        });
        synchronizationPage.box.add(new SynchronizationWidget());


        // scheduled page
        let scheduledPage = this.addPage("schedule", _("Schedule"), {
            vscrollbar_policy: Gtk.PolicyType.NEVER
        });

        let timeSection = scheduledPage.addSection(_("Time"), null, {});

        timeSection.addSetting(_("Time"), null, new TimeSettingWidget({
            spacing: 10
        }));

        let nextSyncLabel = new Gtk.Label({
            label: Settings.getHumanReadableDateOfNextSchedule()
        });
        Settings.gsettings.main.connect("changed::" + Settings.keys.scheduler.NEXT_SYNC, Lang.bind(this, (setting, state, nextSyncLabel) => {
            nextSyncLabel.label = Settings.getHumanReadableDateOfNextSchedule();
        }, nextSyncLabel));

        timeSection.addSetting(_("Upcoming synchronization"), null, nextSyncLabel);

        scheduledPage.addSection(_("Days"), new DayRowSection(), {});


        // About Page
        let aboutPage = this.addPage("about", _("About"), {
            vscrollbar_policy: Gtk.PolicyType.NEVER
        });
        aboutPage.box.add(new AboutWidget());
        aboutPage.box.margin_top = 18;
    }
});

var SynchronizationWidget = new Lang.Class({
    Name: "SynchronizationWidget",
    Extends: Gtk.Box,

    _init() {
        this.parent({
            orientation: Gtk.Orientation.VERTICAL,
            margin_top: 20,
            margin_bottom: 20,
            margin_left: 0,
            margin_right: 0
        });

        // notebook
        this.notebook = new Notebook({
            tab_pos: Gtk.PositionType.LEFT,
        });
        this.add(this.notebook);

        this.groupWidgets = {};

        let groups = Settings.getGroups();
        for (var i = 0, len = groups.length; i < len; i++) {
            this._addGroup(groups[i]);
        }

        this.add(new NewGroupWidget(Lang.bind(this, this.addNewGroup), {
            margin_top: 5,
            hexpand: false,
            halign: Gtk.Align.CENTER,
            valign: Gtk.Align.CENTER
        }));
    },

    addNewGroup(groupName) {
        let groups = Settings.getGroups();
        if (groups.indexOf(groupName) < 0) {
            groups.push(groupName);
            Settings.setGroups(groups);
            Settings.initGroupSetting(groupName);

            this._addGroup(groupName);
        }
    },

    _addGroup(groupName) {
        let widget = this.notebook.addPage(groupName, null, {});
        this.groupWidgets[groupName] = widget;

        // enable
        let mainSection = widget.addSection(groupName, null, {});
        mainSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.ENABLE);

        // sources
        let sourceSection = widget.addSection(_("Sources"), new FolderListSection(
            Settings.gsettings.groups[groupName],
            Settings.keys.group.SOURCES,
            {}
        ));

        // destination;
        let destSection = new DestinationSection(widget, groupName);

        // options
        let optionsSection = widget.addSection(_("Options"), null, {});
        optionsSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.DELETE);
        optionsSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.COMPRESS);
        optionsSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.CUSTOM_OPTIONS);


        // buttons on bottom
        let removeButton = new Gtk.Button({ label: _("Remove this group") + " (" + groupName + ")" });
        widget.box.add(removeButton);
        removeButton.connect("clicked", Lang.bind(this, this.removeGroup, groupName));

        this.show_all();
    },

    removeGroup(button, groupName) {
        this.groupWidgets[groupName].destroy();
        delete this.groupWidgets[groupName];

        let groups = Settings.getGroups();
        let index = groups.indexOf(groupName);
        groups.splice(index, 1);

        Settings.setGroups(groups);
        Settings.gsettings.groups[groupName].reset("");
        delete Settings.gsettings.groups[groupName];
    }
});

var Notebook = new Lang.Class({
    Name: "PreferencesWidgetNotebook",
    Extends: Gtk.Notebook,

    _init (params={}) {
        params = Object.assign({
            can_focus: true,
            valign: Gtk.Align.FILL,
            vexpand: true,
        }, params);
        this.parent(params);
    },

    addPage: function (title, page, params={}) {
        var label;
        if (typeof title === "string") {
            label = new Gtk.Label({
                can_focus: false,
                margin_bottom: 12,
                margin_start: 3,
                xalign: 0,
                use_markup: true,
                label: "<b>" + title + "</b>"
            });
        } else if (title instanceof Gtk.Widget) {
            label = title;
        }

        if (!page) { page = new PreferencesWidget.Page(params); }
        this.append_page(page, label);
        return page;
    }
});

var FolderListSection = new Lang.Class({
    Name: "FolderListSection",
    Extends: PreferencesWidget.Section,

    _init(settings, keyName, params={}) {
        this.parent(params);

        this.settings = settings;
        this.keyName = keyName;

        this.rows = [];

        this.refreshFolders();

        this.settings.connect(
            "changed::" + keyName,
            Lang.bind(this, this.refreshFolders)
        );
    },

    refreshFolders() {
        for (var i = 0, len = this.rows.length; i < len; i++) {
            this.rows.pop().destroy();
        }

        let sources = this.settings.get_strv(this.keyName);

        for (var i = 0, len = sources.length; i < len; i++) {
            this.rows.push(this.addFolder(sources[i]));
        }
        this.rows.push(this.addFolder());

        this.show_all();
    },

    addFolder(folder=null) {
        let row = this.addRow();

        row.folderButton = new Gtk.FileChooserButton({
            action: Gtk.FileChooserAction.SELECT_FOLDER,
            can_focus: true,
            halign: Gtk.Align.FILL,
            hexpand: true,
            valign: Gtk.Align.CENTER,
            visible: true
        });
        row.grid.attach(row.folderButton, 0, 0, 1, 1);

        if (folder != null) {
            row.folderButton.set_filename(folder);

            // let button = new Gtk.Button({ label: "X" });
            let button = new Gtk.Button({});
            button.index = this.rows.length;
            row.grid.attach(button, 1, 0, 1, 1);

            let icon = Gtk.Image.new_from_icon_name("remove", 4);
            button.add(icon);

            button.get_style_context().add_class("circular");

            button.connect("clicked", Lang.bind(this, (button) => {
                this.rows[button.index].destroy();
                this.rows.splice(button.index, 1);
                this.updateSetting();
            }));
        } else {
            let label = new Gtk.Label({
                width_chars: 4,
            });
            row.grid.attach(label, 1, 0, 1, 1);
        }

        row.folderButton.connect("file-set", Lang.bind(this, this.updateSetting));

        return row;
    },

    updateSetting() {
        let result = [];

        for (var i = 0, len = this.rows.length; i < len; i++) {
            let path = this.rows[i].folderButton.get_filename();
            if (path != null) result.push(path);
        }

        this.settings.set_strv(this.keyName, result);

        return result;
    }
});

var DestinationSection = new Lang.Class({
    Name: "DestinationSection",

    _init(widget, groupName) {
        this.destinationSection = widget.addSection(_("Destination"), null, {});
        this.destinationSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.DEST_TYPE);

        this.serverRow = null;
        this.userRow = null;
        this.portRow = null;
        this.folderRow = null;
        this.wolRow = null;

        this.resetDestinationSettings(groupName);
        Settings.gsettings.groups[groupName].connect("changed::" + Settings.keys.group.DEST_TYPE, Lang.bind(this, () => {
            this.resetDestinationSettings(groupName);
        }));

    },

    resetDestinationSettings(groupName) {
        let destinationType = Settings.gsettings.groups[groupName].get_string(Settings.keys.group.DEST_TYPE);
        if ( destinationType == "local" ) {
            if (this.folderRow != null) this.folderRow.destroy();
            if (this.serverRow != null) this.serverRow.destroy();
            if (this.userRow != null) this.userRow.destroy();
            if (this.portRow != null) this.portRow.destroy();
            if (this.wolRow != null) this.wolRow.destroy();

            this.folderRow = this.destinationSection.addRow();
            this.serverRow = null;
            this.userRow = null;
            this.portRow = null;
            this.wolRow = null;

            this.folderRow.folderButton = new Gtk.FileChooserButton({
                action: Gtk.FileChooserAction.SELECT_FOLDER,
                can_focus: true,
                halign: Gtk.Align.FILL,
                hexpand: true,
                valign: Gtk.Align.CENTER,
                visible: true
            });
            this.folderRow.folderButton.connect("file-set", Lang.bind(this, (button) => {
                Settings.setDestinationFolder(groupName, button.get_filename());
            }));
            Settings.gsettings.groups[groupName].connect("changed::" + Settings.keys.group.DEST_FOLDER, Lang.bind(this, () => {
                this.folderRow.folderButton.set_filename(Settings.getDestinationFolder(groupName));
            }));

            let destFolder = Settings.getDestinationFolder(groupName);
            if (destFolder != null && destFolder != "") {
                this.folderRow.folderButton.set_filename(Settings.getDestinationFolder(groupName));
            }

            this.folderRow.grid.attach(this.folderRow.folderButton, 0, 0, 1, 1);
        } else {
            if (this.folderRow != null) this.folderRow.destroy();
            if (this.serverRow != null) this.serverRow.destroy();
            if (this.userRow != null) this.userRow.destroy();
            if (this.portRow != null) this.portRow.destroy();
            if (this.wolRow != null) this.wolRow.destroy();

            this.serverRow = this.destinationSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.DEST_SERVER);
            this.userRow = this.destinationSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.DEST_USER);
            this.portRow = this.destinationSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.DEST_PORT);
            this.folderRow = this.destinationSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.DEST_FOLDER);
            this.wolRow = this.destinationSection.addGSetting(Settings.gsettings.groups[groupName], Settings.keys.group.DEST_WOL);
        }
        this.destinationSection.show_all();
    }

});

var NewGroupWidget = new Lang.Class({
    Name: "NewGroupWidget",
    Extends: Gtk.Box,

    _groupList: [],

    _label: null,
    _entry: null,
    _button: null,

    _callback: null,


    _init(callback, params={}) {
        this.parent(params);

        this._callback = callback;
        this._groupList = Settings.getGroups();

        this._label = new Gtk.Label({
            label: "New group:"
        })
        this.add(this._label);

        this._entry = new Gtk.Entry({
            margin_left: 5
        });
        this.add(this._entry);

        // this._button = new Gtk.Button({
        //     margin_left: 5,
        //     label: "Add"
        // });
        // this.add(this._button);

        this._entry.connect("activate", (entry) => {
            this._activate(entry);
            this.get_toplevel().set_focus(null);
        });

        this._entry.connect("changed", (entry) => {
            if (entry.tex != "" && this._groupList.indexOf(entry.text) < 0) {
                entry.secondary_icon_name = "emblem-ok-symbolic";
            }
        });

        this._entry.connect("icon-release", (entry) => {
            this._activate(entry);
            this.get_toplevel().set_focus(null);
        });

        this._entry.connect("key-press-event", (entry, event, user_data) => {
            if (event.get_keyval()[1] === Gdk.KEY_Escape) {
                entry.text = "";
                entry.secondary_icon_name = "";
                this.get_toplevel().set_focus(null);
            }
        });

        // this._button.connect("clicked", Lang.bind(this, () => {
        //     if (this._entry.text != null && this._entry.text != "") {
        //         this._entry.text = "";
        //         this.addNewGroup(this._entry.text);
        //         this.show_all();
        //     }
        // }));
    },

    _activate(entry) {
        if (entry.text != null && entry.text != "") {
            this._callback(entry.text);
            entry.secondary_icon_name = "";
            entry.text = "";
            this.show_all();
        }
    }
});

var TimeSettingWidget = new Lang.Class({
    Name: "TimeSettingWidget",
    Extends: Gtk.Box,

    _time: null,
    _hourSpinner: null,
    _minuteSpinner: null,

    _init(params={}) {
        this.parent(params);

        this._time = Settings.getScheduledTime();

        this._hourSpinner = this._addNewSpinner();
        this._hourSpinner.adjustment = new Gtk.Adjustment({lower: 0, upper: 23, step_increment: 1});
        this._hourSpinner.set_value(this._time.hour);

        this.add(new Gtk.Label({
            label: ":",
            margin_left: 5,
            margin_right: 5
        }));

        this._minuteSpinner = this._addNewSpinner();
        this._minuteSpinner.adjustment = new Gtk.Adjustment({lower: 0, upper: 59, step_increment: 1});
        this._minuteSpinner.set_value(this._time.min);

        this._hourSpinner.connect("value-changed", Lang.bind(this, this._updateSettings));
        this._minuteSpinner.connect("value-changed", Lang.bind(this, this._updateSettings));
        Settings.gsettings.main.connect("changed::" + Settings.keys.scheduler.TIME, Lang.bind(this, this._updateSpinners));
    },

    _addNewSpinner() {
        let widget = new Gtk.SpinButton({
            //snap_to_ticks: true,
            input_purpose: Gtk.InputPurpose.NUMBER,
            can_focus: true,
            width_request: 80,
            halign: Gtk.Align.END,
            valign: Gtk.Align.CENTER,
            visible: true,
            wrap: true
        });
        this.add(widget);
        return widget;
    },

    _updateSettings() {
        Settings.setScheduledTime(this._hourSpinner.get_value(), this._minuteSpinner.get_value());
    },

    _updateSpinners() {
        let time = Settings.getScheduledTime();
        this._hourSpinner.set_value(time.hour);
        this._minuteSpinner.set_value(time.min);
    }

});

var DayRowSection = new Lang.Class({
    Name: "DayRowSection",
    Extends: PreferencesWidget.Section,

    _switches: {},
    _scheduledDays: [],

    _init(params={}) {
        this.parent(params);

        let scheduledDays = Settings.getScheduledDays();

        for (let i=0; i<Settings.days.length; i++) {
            let day = Settings.days[i];

            this._switches[day] = new Gtk.Switch();
            this._switches[day].set_state((scheduledDays.indexOf(day) >= 0));

            this.addSetting(day, null, this._switches[day]);

            this._switches[day].connect(
                "notify::active",
                Lang.bind(this, this._updateSettings)
            );
            Settings.gsettings.main.connect(
                "changed::" + Settings.keys.scheduler.DAYS,
                Lang.bind(this, this._updateSwitches)
            );
        };
    },

    _updateSwitches() {
        let scheduledDays = Settings.getScheduledDays();
        Object.keys(this._switches).forEach(Lang.bind(this, (day, index) => {
            this._switches[day].set_state((scheduledDays.indexOf(day) >= 0));
        }));
    },

    _updateSettings(widget, state) {
        let scheduledDays = [];
        Object.keys(this._switches).forEach(Lang.bind(this, (day, index) => {
            if (this._switches[day].get_state()) scheduledDays.push(day);
        }));
        Settings.setScheduledDays(scheduledDays);
    }
});

function buildPrefsWidget() {
    let wrsp = new RSyncUIPreferencesWidget();
    GLib.timeout_add(GLib.PRIORITY_DEFAULT, 0, () => {
        let prefsWindow = wrsp.get_toplevel()
        prefsWindow.get_titlebar().custom_title = wrsp.switcher;
        return false;
    });

    wrsp.show_all();
    return wrsp;
}
