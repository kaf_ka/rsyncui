��    P      �  k         �     �  +   �       &        >  A   R     �     �     �     �     �  /   �  0   �     /     4     =  .   D     s          �  "   �  $   �     �     �     �     �     �     	  !   
	     ,	     4	     9	  '   >	     f	  -   �	     �	     �	     �	     �	     �	  #   �	     
     0
     8
     A
     I
     Q
  )   V
     �
  ?   �
  *   �
     �
     	       "     	   >     H     O     W     ^      j     �     �     �     �  	   �     �     �  O   �          2     ;     U     ]  	   i     s     �     �     �  n  �  !   L  7   n     �  ,   �     �  C   �     0     6     ?  
   G  !   R  2   t  0   �     �     �  	   �  1   �  
   #     .  	   7  %   A  (   g  	   �     �     �     �     �     �  !   �     �     �     �  -   �        /   ;     k     q     u     }     �     �  "   �     �     �     �             '        5  4   B  ;   w     �     �     �  "   �  	   �                         +  	   K     U     Z     p     u     �     �  Z   �     �               1     8     D  #   M  %   q     �     �     @   6            C   !      I      L   "   .      %   +   5       4           >             (           B   K       =                     #   	   H          '                         8   J   $         2      N   1   
   9   7            G       /      ?             3       A   ;       F                        M      D   P           0   &   )           E       -   O   :          *   <      ,    "rsync" could not be found. (probably no key pair has been configured). About Activate this group for synchronizing. Alternative options Alternative options for rsync, these replace the default options. April August Close Compress Compress during transmission. Date for upcoming synchronization (dd/mm/yyyyy) Date of most recent synchronization (dd/mm/yyyy) Days December Delete Delete extraneous files from destination dirs. Destination Destination folder. Enable Errors during last synchronization Failures during last synchronization False February Folder Friday Group Host Information about synchronization January July June List of sources that will be processed. Local or remote destination. Mac address of the server. (empty = disabled) March May Monday Most recent failures Most recently synchronized Name or IP address of the computer. No groups to synchronize. Nothing November October Options Port Port on the computer. (0 = standard port) Preferences RSyncUI has completed the synchronization without any problems. RSyncUI has started a new synchronization. Remove this group Saturday Schedule Scheduled days for synchronization September Server Sources Sunday Synchronize There is nothing configured yet. Thursday Time Time for scheduling True Try again Tuesday Type Unfortunately, RSyncUI has encountered problems while running the last command! Upcoming synchronization Username Username on the computer. Version Wake-On-Lan Wednesday does not grant access to user has no destination configured! has no source configured! is not accessible. Project-Id-Version: RSyncUI
POT-Creation-Date: 2018-10-14 09:55+0200
PO-Revision-Date: 2018-10-14 13:16+0200
Last-Translator: 
Language-Team: 
Language: nl_NL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2
X-Poedit-Basepath: ../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: .
 "rsync" kon niet gevonden worden. (waarschijnlijk is er geen sleutelpaar geconfigureerd). Over Activeer deze groep voor het synchroniseren. Alternatieve opties Alternatieve opties voor rsync, deze vervangen de standaard opties. April Augustus Sluiten Comprimeer Comprimeer tijdens het verzenden. Datum voor de volgende synchronisatie (dd/mm/yyyy) Datum van de laatste synchronisatie (dd/mm/yyyy) Dagen December Verwijder Verwijder overtollige bestanden op de bestemming. Bestemming Doelmap. Activeren Fouten tijdens laatste synchronisatie Fouten tijdens de laatste synchronisatie Niet waar Februari Map Vrijdag Groep Computer Informatie over de synchronisatie Januari Juli Juni Lijst van bronnen die verwerkt zullen worden. Lokale bestemming of niet. Mac-addres op de server. (leeg = uitgeschakeld) Maart Mei Maandag Meest recente fouten Laatst gesynchroniseerd Naam of IP van de computer. Geen groepen om te synchroniseren. Niets November Oktober Opties Poort Poort op de server. (0=standaard poort) Instellingen RSyncUI het zijn opdracht voltooid zonder problemen. RSyncUI is begonnen aan een nieuwe synchronisatie opdracht. Verwijder deze groep Zaterdag Plannen Geplande dagen voor synchronisatie September Server Bronnen Zondag Synchronizeren Er is nog niets geconfigureerd. Donderdag Tijd Tijd voor de planning Waar Probeer opnieuw Dinsdag Type Heelaas heeft RSyncUI problemen ondervonden tijdens uit uitvoeren van de laatste opdracht! Volgende synchronisatie Gebruikersnaam Gebruikersnaam op de server. Versie Wake-On-Lan Woensdag verleent geen toegang aan gebruiker er is geen bestemming geconfigureerd! er is geen bron geconfigureerd! is niet bereikbaar. 