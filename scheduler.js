/*
 * RSyncUI
 * An extension to synchronize with rsync
 *
 * This file is part of RSyncUI
 *
 * RSyncUI is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * RSyncUI is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the extension.
 * If not, see <http://www.gnu.org/licenses/>.
 */

const Lang = imports.lang;

const GLib = imports.gi.GLib;

const Extension = imports.misc.extensionUtils.getCurrentExtension();
const Settings = Extension.imports.settings;

var SchedulerUtils = new Lang.Class({
	Name: "SchedulerUtils",

	_callback: null,
	_timer: null,
	_dateOfNextSchedule: null,


	_init() {
        this._dateOfNextSchedule = Settings.findDateOfNextSchedule();

		Settings.gsettings.main.connect('changed::' + Settings.keys.scheduler.DAYS, Lang.bind(this, this.resetSchedulerTimer));
		Settings.gsettings.main.connect('changed::' + Settings.keys.scheduler.TIME, Lang.bind(this, this.resetSchedulerTimer));
    },

	resetSchedulerTimer() {
        Settings.setNextSync("");

        let oldDate = this._dateOfNextSchedule;
        this._dateOfNextSchedule = Settings.findDateOfNextSchedule();

        if (this._dateOfNextSchedule == null) this.stop();
        // else if (oldDate == null || this._dateOfNextSchedule.difference(oldDate) != 0) this.start();
        else this.start();
	},

	setCallback(callback) {
		if (callback === undefined || callback === null || typeof callback !== "function") {
			throw TypeError("'callback' needs to be a function.");
    	}
    	this._callback = callback;
	},

    _callCallback() {
        this._callback();
        this.resetSchedulerTimer();
    },

	start() {
		this.stop();
        let storedDate = Settings.getDateOfNextSchedule();
        let now = new GLib.DateTime();

        let timeout;
        if (storedDate != null && storedDate.difference(now) <= 0) timeout = 0; // when pc was down when event should happen, sync now
        else timeout = Math.round(this._dateOfNextSchedule.difference(now) / 1000);

		this._timer = GLib.timeout_add(GLib.PRIORITY_DEFAULT, timeout, Lang.bind(this, this._callCallback));

		// update the next date for the next synchronization
        Settings.setDateOfNextSchedule(this._dateOfNextSchedule);
	},

	stop() {
		if (this._timer !== null) {
			GLib.source_remove(this._timer);
			this._timer = null;
		}
	}
});
